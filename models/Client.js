const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClientSchema = new Schema({
    clientName: {
        type: String,
        required: true
    },
    dwbLogin: {
        type: Number,
        required: true
    },
    freight: {
        type: Boolean,
        required: true

    },
    dwbPW: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = Client = mongoose.model('clients', ClientSchema);