class Contact {
    constructor(_name, _phone) {
        this.name = _name;
        this.phone = _phone;
    }
}

class PickupAddress {
    constructor(_compName, _streetAdd, _suite, _city, _prov, _postCode, _country, _contact) {
        this.company = _compName;
        this.address = _streetAdd;
        this.suite = _suite;
        this.city = _city;
        this.state = _prov;
        this.postal_code = _postCode;
        this.country = _country;
        this.contact = _contact;
    }
}

class DeliveryAddress extends PickupAddress {
    constructor(_compName, _streetAdd, _suite, _city, _prov, _postCode, _country, _contact, _notes, _reference,
                _specialInstructions, _service_type, _pieces, _weight, _vehicle, _package) {

        super(_compName, _streetAdd, _suite, _city, _prov, _postCode, _country, _contact);

        this.notes = _notes;
        this.reference = _reference;
        this.special_instructions = _specialInstructions;
        this.service_type = _service_type;
        this.number_of_pieces = parseInt(_pieces);
        this.weight = _weight;
        this.package = _package;
        this.vehicle = _vehicle;
    }
}

class Order {
    constructor(_cNo, _cCenter, _orderType, _readyTime, _roundTrip, _pickUpAdd, _deliveryAdd) {
        this.customer_number = _cNo;
        this.cost_center = _cCenter;
        this.order_type = _orderType;
        this.ready_time = _readyTime;
        this.round_trip = _roundTrip;
        this.route_stops = [_pickUpAdd, _deliveryAdd];
    }
}

module.exports = {Order, DeliveryAddress, PickupAddress, Contact};