const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');

const users = require('./routes/users');
const client = require('./routes/client');
const auth = require('./routes/auth');
const orders = require('./routes/orders');
const dwborders = require('./routes/dwborders');

require('dotenv').config();

const app = express();

//depreciated warning fix
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//connect to mongoDB
mongoose
    .connect(process.env.MONGO_URI)
    .then(() => console.log('mongoDB connected'))
    .catch(err => console.error(err));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// passport Middleware
app.use(passport.initialize());

//Passport Config
require('./config/passport')(passport);

app.use('/users', users);
app.use('/client', client);
app.use('/auth', auth);
app.use('/orders', orders);
app.use('/dwborders',dwborders);

const port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log(`Server running on ${port}`);
});

module.exports = {auth};