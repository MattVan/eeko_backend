const axios = require('axios');

const get3PL_orders = async (orderNum, token) => {
    try {
        const response = await axios({
            method: "GET",
            url: `https://secure-wms.com/orders?detail=all&itemdetail=all&rql=referenceNum==${orderNum}`,
            headers: {
                Authorization: token
            }
        })

        if (response.data) {
            return response.data.ResourceList[0];
        } else{
            error = {
                msg:"Not Found"
            }
            return error;
        }
    } catch (e) {
        console.log(e);
    }
}

module.exports = get3PL_orders;