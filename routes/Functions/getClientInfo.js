const axios = require('axios');
const {data} = require('./customerList');

async function getCredentials(name) {
    try {
        const response = await Client.findOne({clientName: name});

        return response;
    } catch (e) {
        console.log(e);
        return null;
    }
}

const getClientInfo = (name) => {

    let res = null;

    for (let i = 0; i < data.length; i++) {
        if (name === data[i].clientName) {
            res = data[i];
            break;
        }
    }

    if (res === null) {
        res = getCredentials(name);
        if (res === null) {
            res = {
                status: 404,
                msg: "Client Not Found"
            }
        }

    }

    return res;
}


module.exports = {
    getClientInfo
}