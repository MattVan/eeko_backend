module.exports = {
    data: [
        {
            "clientName": "Acceso Trading LLC",
            "dwbLogin": 10941,
            "freight": true,
            "dwbPW": "at321"
        }
        ,
        {
            "clientName": "Brutus",
            "dwbLogin": 10828,
            "freight": true,
            "dwbPW": "bbi123"
        },
        {
            "clientName": "Bittered Sling",
            "dwbLogin": 10918,
            "freight": true,
            "dwbPW": "bit123"
        },
        {
            "clientName": "Brasa Peruvian Kitchen",
            "dwbLogin": 10918,
            "freight": true,
            "dwbPW": "bit123"
        },
        {
            "clientName": "CAF",
            "dwbLogin": 10316,
            "freight": true,
            "dwbPW": "wca123",
            "costCenter":"Canadian Artisan Foods"
        },
        {
            "clientName": "Cosman & Webb",
            "dwbLogin": 10849,
            "freight": true,
            "dwbPW": "cw123"
        },
        {
            "clientName": "Crazy D's All Natural Labs Inc",
            "dwbLogin": 10942,
            "freight": true,
            "dwbPW": "crazy123"
        },
        {
            "clientName": "Eve's Crackers",
            "dwbLogin": 10862,
            "freight": true,
            "dwbPW": "ec321"
        },
        {
            "clientName": "DomenicaFiore",
            "dwbLogin": 10373,
            "freight": true,
            "dwbPW": "dfc123"
        },
        {
            "clientName": "Fatso",
            "dwbLogin": 10823,
            "freight": true,
            "dwbPW": "1234"
        },
        {
            "clientName": "Natures Bodega",
            "dwbLogin": 10949,
            "freight": true,
            "dwbPW": "1234"
        },
        {
            "clientName": "Sacred Foods",
            "dwbLogin": 10916,
            "freight": false,
            "dwbPW": "sf321"
        },
        {
            "clientName": "QMS",
            "dwbLogin": 10419,
            "freight": false,
            "dwbPW": "qms123"
        }
    ]
}