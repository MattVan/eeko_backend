const axios = require('axios');

require('dotenv').config();
const Client = require('../../models/Client');

const body = {
    "grant_type": process.env.GRANT_TYPE,
    "tpl": process.env.TPL,
    "user_login_id": process.env.USER_LOGIN_ID
}

const tokenHeaders = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json",
    "Host": "secure-wms.com",
    "Accept-Language": "Content-Length",
    "Accept-Encoding": "gzip,deflate,sdch",
    "Authorization": process.env.AUTH_3PL_BASIC
}

async function getAuth() {
    try {
        const response = await axios({
            method: 'post',
            url: 'https://secure-wms.com/AuthServer/api/Token',
            data: body,
            headers: tokenHeaders
        });

        return response.data;

    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    getAuth
};