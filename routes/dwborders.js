/*
    This JS route file handles all functionality with digital waybill side of the orders -
     specifically placing an order and getting that orders price after
 */
const express = require('express');
const router = express.Router();
const axios = require('axios');

const {Contact} = require("../classes/Order");

const {getClientInfo} = require('./Functions/getClientInfo');
require('dotenv').config();

const {PickupAddress, DeliveryAddress, Order} = require('../classes/Order');


router.get('/test', ((req, res) => {

    let now = new Date();

    console.log(now.get);
    res.send(now.toString());
}))

const daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const formatDate = (weekday, date, month, year, time) => {
    let day = daysOfWeek[parseInt(weekday)];

    if (parseInt(date) < 10) {
        return `${day}, 0${date} ${months[month]} ${year} ${time}:00:00`;
    } else {
        return `${day}, ${date} ${months[month]} ${year} ${time}:00:00`;
    }
}

router.post('/placeorder/:customerName', (req, res) => {
    // console.log(req.body);
    let pickupAdd;

    console.log(`sending order ${req.body.reference}`);

    pickupAdd = new PickupAddress(
        "Eeko West Warehouse",
        "669 Ridley Place",
        "206",
        "Delta",
        "BC",
        "V3M 6Y9",
        "Canada",
        new Contact(
            "Martin Casuga",
            "778-862-7124"
        ));

    const delAdd = new DeliveryAddress(
        req.body.companyName,
        req.body.stAdd1,
        req.body.suite || "",
        req.body.city,
        req.body.province,
        req.body.postCode,
        req.body.country,
        new Contact(
            req.body.contactName,
            req.body.contactNumber
        ),
        req.body.instructions,
        req.body.reference,
        req.body.instructions,
        req.body.service,
        req.body.pieces,
        req.body.weight,
        req.body.vehicle,
        req.body.package
    );

    let custInfo = getClientInfo(req.params.customerName);

    let now = new Date();

    let formatted = formatDate(now.getDay(), now.getDate(), now.getMonth(), now.getFullYear(), now.getHours());

    // console.log(formatted);

    const newOrder = new Order(
        custInfo.dwbLogin,
        custInfo.costCenter,
        "Delivery",
        formatted,
        false,
        pickupAdd,
        delAdd
    );

    // console.log(newOrder);

    axios({
        method: "POST",
        url: `http://api.dwaybill.com/${process.env.CID}/orders.json?v=1&key=${process.env.QUICK_ENTRY_KEY}&customer_number=${custInfo.dwbLogin}&password=${custInfo.dwbPW}`,
        data: newOrder
    })
        .then(result => {
            // console.log(result);
            console.log(`finished order ${req.body.reference} successfully`);
            res.send(result.data.body);
        })
        .catch(e => {
            console.log(e);
            res.send(e);
        });
});

router.get('/:id', (req, res) => {
    console.log(`sending order ${req.params.id} to get price`);

    axios({
        method: "get",
        url: `http://api.dwaybill.com/${process.env.CID}/orders.json/${req.params.id}?v=1&key=${process.env.REMOTE_KEY}`
    })
        .then(async result => {
            await Promise.resolve(result);
            // console.log(result);

            const data = {
                price: result.data.body.price,
                status:result.data.status
            };

            res.send(data);
        })
        .catch(error => {
            console.log(error, "line 127 dwb");
            // console.log(error.message);
            res.send(error);
        })
})

module.exports = router;