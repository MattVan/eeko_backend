/*
    This js route handles all the order related items with 3pl, more specifically the router.post
     /:id is the main functionality
 */

const express = require('express');
const router = express.Router();
const axios = require('axios');
const get3PL_orders = require("./Functions/getOrders3PL");
const {getAuth} = require("./Functions/authenticate");

router.get('/test', (req, res) => {
    res.json({msg: 'works'});
})


//Fix this to accept an array of items instead of one
router.post('/getorders', (req, res) => {
    let token;
    let promises = [];

    try {
        getAuth()
            .then(result => {
                token = "Bearer " + result.access_token
            })
            .then(async () => {
                for (let i = 0; i < req.body.so.length; i++) {
                    const thisPromise = get3PL_orders(req.body.so[i], token);
                    promises.push(thisPromise);
                }

                let response = await Promise.all(promises);
                // console.log(response);
                res.send(response);
            })
    } catch (e) {
        console.log(e);
    }
})

module.exports = router;