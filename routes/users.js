//Currently working as intended

const express = require('express');
const router = express.Router();

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const passport = require('passport');

require('dotenv').config();

const User = require('../models/User');

// @route GET /users/test
// @desc Tests users route
// @access Public
router.get('/test', (req, res) => {
    res.json({msg: 'users works'});
});

// @route POST /users/register
// @desc Register a user
// @access Public
router.post('/register', (req, res) => {
    User.findOne({name: req.body.name})
        .then(user => {
            if (user) {
                return res.status(400).json({name: 'Name already exists'});
            } else {
                const newUser = new User({
                    name: req.body.name,
                    password: req.body.password
                });

                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;

                        newUser.save()
                            .then(user => res.json(user))
                            .catch(err => {
                                console.error(err);
                            })
                    })
                })
            }
        })
        .catch(err => console.error(err))
});

// @route POST users/login
// @desc Login User / Returns JWT Token
// @access Public
router.post('/login', (req, res) => {
    const _name = req.body.name;
    const password = req.body.password;

    User.findOne({name:_name})
        .then(user => {
            if (!user) {
                return res.status(404).json({msg: "User not found"});
            }
            //check password
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if (isMatch) {
                        //user matched, create payload
                        const payload = {
                            id: user.id,
                            name: user.name
                        };
                        //token good for one day
                        jwt.sign(
                            payload,
                            process.env.SECRET_OR_KEY,
                            {expiresIn: 21600},
                            (err, token) => {
                                res.json({
                                    success: true,
                                    token: 'Bearer ' + token,
                                    expiry: Date.now()
                                })
                            });
                    } else {
                        return res.status(400).json({msg: "Incorrect Password"});
                    }
                })
                .catch(err=>console.log(err));
        })
        .catch(err => {
            console.error(err)
        });

});

module.exports = router;