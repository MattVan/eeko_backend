//Need to fix this when working on client route

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const bcrypt = require('bcrypt');

const Client = require('../models/Client');

// @route GET client/test
// @desc Tests client route
// @access Public
router.get('/test', (req, res) => res.json({msg: 'Client Works'}));

// @route GET client/getclients
// @desc Gets all clients from the database
// @access Private
router.get(
    '/getallclients',
    passport.authenticate('jwt', {session: false}),
    (req, res) => {
        const errors = {};
        Client.find().exec()
            .then(result => {
                //result is returned as an array because we are getting everything, so just checking for empty array
                if (result.length === 0) {
                    errors.noclients = 'There are no clients in the database';
                    return res.status(404).json(errors);
                }
                //Automatically sends status 200 here
                res.json(result);
            })
            .catch(err => res.status(404).json(err));
    }
);
// @route GET client/getclient
// @desc Gets one clients from the database
// @access Private
router.get(
    '/getclient',
    passport.authenticate('jwt', {session: false}),
    (req, res) => {
        let name = req.body.clientName;
        const errors = {};
        Client.findOne({clientName:name})
            .then(client => {

                if (!client) {
                    errors.noclients = 'Client doesnt exist in the db';
                    return res.status(404).json(errors);
                }
                //Automatically sends status 200 here
                res.json(client);
            })
            .catch(err => res.status(404).json(err));
    }
);

// @route POST client/newclient
// @desc creates a new client for the database
// @access Private
router.post(
    '/newclient',
    passport.authenticate('jwt', {session: false}),
    (req, res) => {

        console.log("Beginning client creation");

        const name = req.body.clientName;
        const loginName = req.body.dwbLogin;
        const pw = req.body.dwbPW;
        const _freight = req.body.freight;

        //Try to find client, if it doesn't exist already we can create a new one
        Client.findOne({clientName: name})
            .then(client => {
                if (client) {
                    return res.status(400).json({msg: "That client already exists"});
                } else {
                    const newClient = new Client({
                        clientName: name,
                        dwbLogin: loginName,
                        freight: _freight,
                        dwbPW: pw
                    });

                    newClient.save()
                        .then(result=>res.json(result.data))
                        .catch(err=>console.log("Error server side creating client", err))
                }
            })
            .catch(err=>console.log("Error creating client: ", err));
    }
);

// @route UPDATE client/updateclient/:client
// @desc Updates a client's information
// @access Private
router.put(
    '/api/client/updateclient',
    passport.authenticate('jwt', {session: false}),
    (req, res) => {
        const errors = {};

        //????? maybe?  Need a way to identify before updating in case the name is updated
        //note to self - may come back here to fix logic based on front end implementation
        const oldName = req.body.oldName;

        const name = req.body.clientName;
        const loginName = req.body.clientLogin;
        const pw = req.body.dwbPW;

        //uses old name regardless of whether it is updated or not
        Client.findOneAndUpdate(
            {
                clientName: oldName
            },
            {
                clientName: name,
                clientLogin: loginName,
                dwbPW: pw
            },
            {
                returnNewDocument: true
            })
            .then(result => {

                res.json(result);
            })
            .catch(err => console.error(err));
    }
)

module.exports = router;