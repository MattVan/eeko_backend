const express = require('express');
const router = express.Router();

const getAuth = require('./Functions/authenticate');

//This route will auth 3PL but not really necessary, come back later.
router.get('/3plbasic', ((req, res) => {

    getAuth().then(r => {
        res.send(r)
    });

}))


module.exports = router;